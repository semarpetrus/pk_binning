#!/usr/bin/env python
from pk_bin import bin_assembly, print_err
import argparse
from os.path import join, exists, isfile, isdir
from os import mkdir, listdir
import datetime
import sys

def main():
    parser = argparse.ArgumentParser(
        add_help=True)
    parser.add_argument(
        '-a', '--assembly', metavar='String', type=str, action='store',
        dest='inputfile', required=True)
    parser.add_argument(
        '-p', '--prefix', metavar='String', type=str, action='store',
        dest='prefix', required=True)
    parser.add_argument(
        '-o', '--outdir', metavar='String', type=str, action='store',
        dest='outdir', required=True)
    parser.add_argument(
        '-i', '--idxdir', metavar='String', type=str, action='store',
        dest='idx_dir', required=True)
    parser.add_argument(
        '-s', '--idxsizes', metavar='String', type=str, action='store',
        dest='idx_sizes', required=True)
    parser.add_argument(
        '-c', '--cpu', metavar='INT', type=int, action='store',
        dest='cores', default=1)
    parser.add_argument(
        '-x', '--threshold', metavar='INT', type=int, action='store',
        dest='threshold', default=50)


    args = parser.parse_args()

    # Command line arguments
    inputfile = args.inputfile
    prefix = args.prefix
    outdir = args.outdir
    index_dir = args.idx_dir
    index_sizes = args.idx_sizes
    cores = args.cores
    threshold = args.threshold/100

    # Make the output directory if it doesn't exist
    if not exists(outdir):
        mkdir(outdir)

    # Bin assembly
    fin = bin_assembly(inputfile, outdir, prefix, index_dir, index_sizes, threshold, cores)

if __name__ == "__main__":
    main()
