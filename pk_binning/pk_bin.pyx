import gzip
from Bio import SeqIO
import datetime
import sys
import pandas as pd
import io
from os.path import join
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp.unordered_map cimport unordered_map
from libc.string cimport strcpy, strlen
from libc.stdio cimport printf
from concurrent.futures import ThreadPoolExecutor
import numpy as np
import plotly.express as px
from multiprocessing import Pool

def print_err(message):
    print(f"{datetime.datetime.now()}: {message}")


# def genome_name(genome):
#     return genome.split('/')[-1].split('.')[0]


def get_position_score(genome_file,  kmers, gnum):
    contigs_pos = {}
    threads = 1
    seqs = []
    k = 31
    zero_score = 1<<gnum
    N_score  = (1<<(gnum+1))-1
    # print_err(f"Scoring {genome_file}.")
    with gzip.open(genome_file, 'rt') as infile:
        for c, record in enumerate(SeqIO.parse(infile, 'fasta')):
            # print(record.id)
            seq = bytes(str(record.seq).upper(), 'ascii')
            kmer_vector = break_seq_with_Ns(seq)
            contigs_pos[record.id] = []
            for kmer_v in kmer_vector:
                if kmer_v not in kmers:
                    # Case non ACGT base
                    if kmer_vector[kmer_v] == 0:
                        contigs_pos[record.id].append(N_score)
                    # Case reference Kmer not in Index
                    else:
                        contigs_pos[record.id].append(zero_score)
                else:
                    contigs_pos[record.id].append(kmers[kmer_v])
    # print_err(f"Finished scoring {genome_name(genome_file)}.")

    return contigs_pos


cpdef vector[unsigned long long] break_seq(char * seq) nogil:
    cdef unordered_map[char, unsigned long long] btod, comp
    cdef int i, seq_size, x, N_count, k
    cdef char base
    cdef unsigned long long digiqry, digirev, digimax, kmers_default, kmers_template, kbit, rev_i, num, revnum
    cdef vector[unsigned long long] digimax_vec
    kmers_template = 3
    k = 31
    kmers_default = 1
    btod[b'A'] = 3
    btod[b'C'] = 2
    btod[b'G'] = 1
    btod[b'T'] = 0
    comp[b'A'] = 0
    comp[b'C'] = 1
    comp[b'G'] = 2
    comp[b'T'] = 3
    digiqry = 0
    digirev = 0
    N_count = 0
    seq_size = strlen(seq)
    for i in range(k):
        kmers_default = kmers_default<<2
    for i in range(k-2):
        kmers_template = kmers_template<<2 | 3
    for i in range(k):
        base = seq[i]
        N_count -= 1
        if btod.find(base) == btod.end():
            N_count = k
            base = b'A'
        num = btod[base]
        revnum = comp[base]
        rev_i = revnum<<((k-1)*2)
        digiqry = (kmers_default | ((digiqry & kmers_template)<<2)) | num
        digirev = (kmers_default | ((digirev>>2) & kmers_template)) | rev_i

    if N_count <= 0:
        if digiqry >= digirev:
            digimax = digiqry
        else:
            digimax = digirev
        digimax_vec.push_back(digimax)

    for i in range(k, seq_size):
        base = seq[i]
        N_count -= 1
        if btod.find(base) == btod.end():
            N_count = k
            base = b'A'
        num = btod[base]
        revnum = comp[base]
        rev_i = revnum<<((k-1)*2)
        digiqry = (kmers_default | ((digiqry & kmers_template)<<2)) | num
        digirev = (kmers_default | ((digirev>>2) & kmers_template)) | rev_i

        if N_count <= 0:
            if digiqry >= digirev:
                digimax = digiqry
            else:
                digimax = digirev
            digimax_vec.push_back(digimax)
    # printf("%s\n", "fin")

    return digimax_vec


cpdef vector[unsigned long long] break_seq_with_Ns(char * seq) nogil:
    cdef unordered_map[char, unsigned long long] btod, comp
    cdef int i, seq_size, x, N_count, k
    cdef char base
    cdef unsigned long long digiqry, digirev, digimax, kmers_default, kmers_template, kbit, rev_i, num, revnum
    cdef vector[unsigned long long] digimax_vec
    kmers_template = 3
    k = 31
    kmers_default = 1
    btod[b'A'] = 3
    btod[b'C'] = 2
    btod[b'G'] = 1
    btod[b'T'] = 0
    comp[b'A'] = 0
    comp[b'C'] = 1
    comp[b'G'] = 2
    comp[b'T'] = 3
    digiqry = 0
    digirev = 0
    N_count = 0
    seq_size = strlen(seq)

    for i in range(k):
        kmers_default = kmers_default<<2
    for i in range(k-2):
        kmers_template = kmers_template<<2 | 3
    for i in range(k):
        base = seq[i]
        N_count -= 1
        if btod.find(base) == btod.end():
            N_count = k
            base = b'A'
        num = btod[base]
        revnum = comp[base]
        rev_i = revnum<<((k-1)*2)
        digiqry = (kmers_default | ((digiqry & kmers_template)<<2)) | num
        digirev = (kmers_default | ((digirev>>2) & kmers_template)) | rev_i

    if N_count <= 0:
        if digiqry >= digirev:
            digimax = digiqry
        else:
            digimax = digirev
        digimax_vec.push_back(digimax)
    else:
        digimax_vec.push_back(0)


    for i in range(k, seq_size):
        base = seq[i]
        N_count -= 1
        if btod.find(base) == btod.end():
            N_count = k
            base = b'A'
        num = btod[base]
        revnum = comp[base]
        rev_i = revnum<<((k-1)*2)
        digiqry = (kmers_default | ((digiqry & kmers_template)<<2)) | num
        digirev = (kmers_default | ((digirev>>2) & kmers_template)) | rev_i

        if N_count <= 0:
            if digiqry >= digirev:
                digimax = digiqry
            else:
                digimax = digirev
            digimax_vec.push_back(digimax)
        else:
            digimax_vec.push_back(0)

    # printf("%s\n", "fin")

    return digimax_vec


cpdef vector[unsigned long long] break_seq_wrap(char * s):
    cdef vector[unsigned long long] digivec
    with nogil:
        digivec = break_seq(s)

    return digivec


def get_kmer_position_cython(cohort, cohort_num, genome_file, genomes_count):
    threads = 1
    seqs = []
    k = 31
    kmers = {}
    score = 1<<genomes_count
    score_template = k<<(2*(k-2))
    # print_err(f"Scoring {genome_file}.")
    cohorts = set()
    for c in cohort:
        cohorts.add(c<<(2*(k-2)))
    with gzip.open(genome_file, 'rt') as infile:
        for c, record in enumerate(SeqIO.parse(infile, 'fasta')):
#             print(record.id)
            seq = bytes(str(record.seq).upper(), 'ascii')
            seqs.append(seq)

            if len(seqs) == threads:
                with ThreadPoolExecutor(threads) as executor:
                    results = executor.map(break_seq_wrap, seqs)
                    for result in results:
                        for digimax in result:
                            score_lead = digimax & score_template
                            if score_lead in cohorts:
                                kmers[digimax] = score
                    seqs = []
        if seqs:
            with ThreadPoolExecutor(threads) as executor:
                results = executor.map(break_seq_wrap, seqs)
                for result in results:
                    for digimax in result:
                        score_lead = digimax & score_template
                        if score_lead in cohorts:
                            kmers[digimax] = score
    # print_err(f"Finished scoring {genome_name(genome_file)}.")

    return kmers


def get_scores_bool(scores, gnum):
    scores_bd = []
    for score in scores:
        if score == 0:
            scores_bd.append([0 for i in range(gnum)])
        else:
            scores_bd.append([int(i) for i in list(bin(score)[3:])])
    np_arr = np.array(scores_bd).astype(int)

    return np_arr


def get_contigs_scores_kmers(list genomes, index_dir, genome_file, cores):
    cdef list cohorts = []
    kmers = {}
    gnum = len(genomes)
    ########### Cohorts Setup ##############
    cohorts = [set([16, 17, 18, 19]),
              set([20, 21, 22, 23]),
              set([24, 25]), set([26, 27]),
              set([28]), set([29]), set([30]), set([31])]
    ########## Set up Process Data ##########
    proc_data = [[d, c, genomes, index_dir, genome_file] for c, d in enumerate(cohorts)]
    p = Pool(cores)
    kmer_sets = p.map(get_scored_kmers, proc_data)
    for kset in kmer_sets:
        print_err("Updating Kmers Dictionary")
        # print(len(kset))
        kmers.update(kset)
    ######### Get positional scores ##########
    # print(len(kmers))
    contigs_pos = get_position_score(genome_file, kmers, gnum)

    return contigs_pos, kmers


def get_scored_kmers(args):
    cohort, c_count, genomes, index_dir, genome_file = args
    k = 31
    genomes_count = len(genomes)
    score_bitsize = int((genomes_count+1)/8)+1
    kmer_bitsize  = int((k+1)*2/8)+1
    scores_file = f"{index_dir}/scores_{c_count}.b.gz"
    kmers_file = f"{index_dir}/kmers_{c_count}.b.gz"
    print_err(f"Starting cohort {cohort} in {genome_file}")
    kmers = get_kmer_position_cython(cohort, c_count, genome_file, genomes_count)
#     print_err(f'Kmers_length 1: {len(kmers)}')
    kmers = read_indexes(kmers, kmers_file, scores_file, kmer_bitsize, score_bitsize)
    print_err(f"{len(kmers)} Found in cohort {cohort} in {genome_file}")
#     print_err(f'Kmers_length 2: {len(kmers)}')

    return kmers


def read_indexes(kmers, kmers_file, scores_file, kmer_bitsize, score_bitsize):
    print(datetime.datetime.now())
    with gzip.open(kmers_file, "rb") as kmer_in:
        with gzip.open(scores_file, "rb") as score_in:
            score_buffer = io.BufferedReader(score_in)
            kmer_buffer  = io.BufferedReader(kmer_in)
            score = score_buffer.read(score_bitsize)
            kmer = kmer_buffer.read(kmer_bitsize)
            while score:
                kmerint = int.from_bytes(kmer, 'big', signed=False)
                if kmerint in kmers:
                    scoreint = int.from_bytes(score, 'big', signed=False)
                    kmers[kmerint] = scoreint
                score = score_buffer.read(score_bitsize)
                kmer = kmer_buffer.read(kmer_bitsize)

    print(datetime.datetime.now())

    return kmers


def make_plot(pm_df, prefix, outdir):
    """
    """
    print_err('Creating plot')
    fig = px.bar(pm_df['match'])
    fig.update_layout(
        showlegend=False,
        xaxis=dict(title='Reference Organism'),
        yaxis=dict(title='Similarity (Kmers)'))
    fig.write_html(join(outdir, f"{prefix}_ref_cov.html"))

    return


def get_query_contigs(assembly):
    """
    """
    contigs = {}
    with gzip.open(assembly, 'rt') as infile:
        for record in SeqIO.parse(infile, 'fasta'):
            contigs[record.id] = record.seq

    return contigs


def get_ref_info(sizes_tsv):
    """
    """
    df = pd.read_csv(sizes_tsv, sep="\t", header=None, names=['ref', 'total'])
    df['output'] = df.ref + ".fasta"

    return df.set_index('ref')


def read_metadata(index_dir):
    """
    """
    metadata_file = join(index_dir, 'metadata.txt')
    metadata_df   = pd.read_csv(metadata_file, sep=':', names=['name', 'value'])
    genome_files       = metadata_df.loc[metadata_df['name'] == 'genomes', 'value'].values[0].split(',')
    genome_orgs = [genome.split('/')[-1].split('.')[0] for genome in genome_files]

    return genome_files, genome_orgs


def get_scores(genome_files, genome_names, kmers, ref_genomes_df, threshold):
    """
    """
    print_err('Starting Kmers count')
    np_arr = np.zeros([1, len(genome_files)])
    for kmer in kmers:
        np_arr = np.add(np_arr, get_scores_bool([kmers[kmer]], len(genome_files)))

    print_err('Finishing Kmers count')
    pmatch_df = pd.DataFrame(np_arr, columns=genome_names[::-1]).transpose()
    pmatch_df.columns = ['match']
    pmatch_df = pmatch_df.join(ref_genomes_df)
    pmatch_df['perc'] = pmatch_df.apply(lambda x: x[0]/x[1], axis =1)

    org_present = list(pmatch_df.loc[pmatch_df['perc'] >= threshold].index)
    org_present_idx = [genome_names[::-1].index(i) for i in org_present]
    if org_present:
        for org in org_present:
            print_err(f"############## Organism Present: {org} ###############")
    else:
        print_err("#########################################################")
        print_err("################## No Organism Present ##################")
        print_err("#########################################################")

    print_err('Creating % match table')
    pmatch_df.to_csv('pmatch.tsv', sep='\t')

    return org_present, org_present_idx, pmatch_df


def Reverse(lst):
    return [ele for ele in reversed(lst)]


def bin_assembly(inputfile, outdir, prefix, index, sizes, threshold, cores):
    # Get reference sizes
    ref_genomes_df = get_ref_info(sizes)

    # # # Get output file names
    output_files = ref_genomes_df.to_dict()['output']

    # # # Load-in sequences
    contig_seqs = get_query_contigs(inputfile)

    # # Read metadata file
    idx_files, idx_genomes = read_metadata(index)
    rev_idx_genomes = Reverse(idx_genomes)

    # # Get contigs' scores and Kmers
    contigs, kmers = get_contigs_scores_kmers(idx_files, index, inputfile, cores)
    org_present, org_present_idx, pmatch_df = get_scores(idx_files, idx_genomes, kmers, ref_genomes_df, threshold)

    # # # Save plot
    make_plot(pmatch_df, prefix, outdir)


    # # # Convert Kmer level scores to base level scores
    blevel_scores = {}
    for contig in contigs:
        tmp_scores = [0 for i in range(30)]
        for c, s in enumerate(contigs[contig]):
            for i in range(30):
                tmp_scores[c+i] = tmp_scores[c+i] | s
            tmp_scores.append(s)
        blevel_scores[contig] = tmp_scores

    # # # Write kmer match        
    print_err('Creating kmer match table')
    df = pd.DataFrame(columns = rev_idx_genomes)

    for contig in contigs:
        sums = get_scores_bool(blevel_scores[contig], len(idx_files))
        s = sums.sum(axis=0)
        df.loc[len(df)] = s

    df['contigs'] = list(contigs.keys())
    df = df.set_index('contigs')
    df.to_csv('kmer_match.tsv', sep='\t')


    # # # Write contigs
    if not org_present:
        # Copy assembly to "unidentified"
        with open(join(outdir, f"{prefix}_unidentified.fasta"), "wt") as outfile:
            for contig in contig_seqs:
                outfile.write(f'>{contig}\n')
                outfile.write(f'{contig_seqs[contig]}\n')

    elif len(org_present) == 1:
        # Copy assembly to "ref_org.fasta"
        ofile = join(outdir, f"{prefix}_{output_files[idx_genomes[::-1][org_present_idx[0]]]}")
        with open(ofile, "wt") as outfile:
            for contig in contig_seqs:
                outfile.write(f'>{contig}\n')
                outfile.write(f'{contig_seqs[contig]}\n')

    else:  
        # Split each contig to the organism it belongs to
        print("############# % KMER MATCH ##############")
        binsH = {}
        binsM = {}
        binsL = {}
        scoreH = pd.DataFrame(columns= ['contig', 'org', 'score'])
        for contig in contigs:
            binsH[contig] = []
            binsM[contig] = []
            binsL[contig] = []

            sums = get_scores_bool(blevel_scores[contig], len(idx_files))
            s = sums.sum(axis=0)

            temp = 0
            for i in org_present_idx:
                if s[i]/len(blevel_scores[contig]) >= 0.5:
                    if s[i]/len(blevel_scores[contig]) > temp:
                        binsH[contig].append(i)
                        temp = s[i]/len(blevel_scores[contig])
                elif s[i]/len(blevel_scores[contig]) < 0.5 and s[i]/len(blevel_scores[contig]) >= 0.2:
                    binsM[contig].append(i)
                elif s[i]/len(blevel_scores[contig]) < 0.2:
                    binsL[contig].append(i)

        print("################## SORTING ##################")
        for contig in binsH:
            if binsH[contig]:
                for file in binsH[contig]:
                    ofile = join(outdir, f"{prefix}_{output_files[idx_genomes[::-1][file]]}")
                    with open(ofile, 'at') as outfile:
                        outfile.write(f'>{contig}\n')
                        outfile.write(f'{contig_seqs[contig]}\n')

        for contig in binsM:
            if binsM[contig]:
                for file in binsM[contig]:
                    ofile = join(outdir, f"{prefix}_{output_files[idx_genomes[::-1][file]]}")
                    with open(ofile, 'at') as outfile:
                        outfile.write(f'>{contig}\n')
                        outfile.write(f'{contig_seqs[contig]}\n')

        for contig in binsL:
            if binsL[contig]:
                with open(join(outdir, f"{prefix}_unidentified.fasta"), "wt") as outfile:
                    outfile.write(f'>{contig}\n')
                    outfile.write(f'{contig_seqs[contig]}\n')

        # for contig in bins:
        #     if bins[contig]: # write present organism files
        #         for file in bins[contig]:
        #             ofile = join(outdir, f"{prefix}_{output_files[idx_genomes[::-1][file]]}")
        #             with open(ofile, 'at') as outfile:
        #                 outfile.write(f'>{contig}\n')
        #                 outfile.write(f'{contig_seqs[contig]}\n')
        #     else: # write other organism files
        #         for file in org_present_idx:
        #             ofile = join(outdir, f"{prefix}_{output_files[idx_genomes[::-1][file]]}")
        #             with open(ofile, 'at') as outfile:
        #                 outfile.write(f'>{contig}\n')
        #                 outfile.write(f'{contig_seqs[contig]}\n')

    return True
