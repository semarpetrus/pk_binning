from setuptools import setup
from Cython.Build import cythonize
from distutils.extension import Extension
from Cython.Distutils import build_ext

setup(
    name='pk_binning',
    version='0.0.3',
    description='Bin assembly to reference genomes using PanKmer' ,
    install_requires=[
        "argparse", "biopython", "setuptools", "wheel", "Cython"],
    long_description=open('README.md').read(),
    packages=["pk_binning"],
    entry_points = {
        'console_scripts': ['pk_binning=pk_binning.pk_binning:main'],
    },
    ext_modules =[ Extension('pk_bin',
        sources=["pk_binning/pk_bin.pyx"], language='c++',
        extra_compile_args = ["-std=c++11"])],
    cmdclass = {'build_ext': build_ext}
)
